const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const promptly = require('promptly');
const argv = require('yargs').argv,
	host = argv.host || 'http://localhost:3000',
	pagePath = argv.page || '/',
	image = argv.image || 'image';

const iPhone = devices['iPhone 8'];
const iPadLanscape = devices['iPad landscape'];

console.log(`Starting ipad and iphone x screenshots for "${host}${pagePath}"`);

let responses = {};

const screenshot = async (screen) => {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	await page.emulate(screen);
	await page.goto(host, { waitUntil: 'networkidle2' });
	const inputs = await page.$$('input');
	for (const input of inputs) {
		let p = await input.getProperty('id');
		let id = await p.jsonValue();
		let d = await input.getProperty('type');
		let type = await d.jsonValue();
		if (responses[id]) {
			await input.type(responses[id]);
		} else {
			let response = type === 'password' ? await promptly.password(`${id}:`, {replace: '*'}) : await promptly.prompt(`${id}:`);
			responses[id] = response;
			await input.type(response);
		}
		
	}
	console.log(`starting screenshot screen: ${screen.name}`);
	const np = page.waitForNavigation();
	await inputs[inputs.length-1].press('Enter');
	await np;
	await page.goto(`${host}${pagePath}`, { waitUntil: 'networkidle2' });
	await page.waitFor(10000);
	let path = `${image}-${screen.name.replace(/([^a-z])/i,'-')}.png`;
	await page.screenshot({path});
	console.log(`completed screenhot: ${path}`);

	await browser.close();
}

(async () => {
	await screenshot(iPadLanscape);
	await screenshot(iPhone);
})();
