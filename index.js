const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const argv = require('yargs').argv,
	url = argv.url || 'http://localhost:3000',
	image = argv.image || 'image';

const iPhone = devices['iPhone 8'];
const iPadLanscape = devices['iPad landscape'];
const iPadProLanscape = devices['iPad Pro landscape'];

console.log(`Starting ipad and iphone x screenshots for "${url}"`);

const screenshot = async (screen) => {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	await page.emulate(screen);
	await page.goto(url, { waitUntil: 'networkidle2' });
	console.log(`starting screenshot screen: ${screen.name}`);
	let path = `${image}-${screen.name.replace(/([^a-z0-9])/ig,'-')}.png`;
	await page.screenshot({path});
	console.log(`completed screenhot: ${path}`);
	await browser.close();
};


(async () => {
	await screenshot(iPadLanscape);
	await screenshot(iPhone);
	await screenshot(iPadProLanscape);
})();
